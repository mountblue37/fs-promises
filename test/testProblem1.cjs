const problem1 = require('../problem1.cjs')

let random = Math.floor(Math.random() * 100)
const fileNames = Array(random).fill(0).map((value, index) => {
    return `file-${index}.json`
})

problem1(fileNames, 'randomFiles')