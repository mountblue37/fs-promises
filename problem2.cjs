const fs = require('fs')
const path = require('path')

function readFile(fileName) {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(__dirname, fileName), 'utf-8', (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(data)
                console.log(`${fileName} has been read successfully`)
            }
        })
    })
}

function writeFile(fileName, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path.join(__dirname, fileName), data, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve(fileName)
                console.log(`${fileName} has been written successfully`)
            }
        })
    })
}

function appendFile(fileName, fileToAppend) {
    return new Promise((resolve, reject) => {
        fs.appendFile(path.join(__dirname, fileName), `${fileToAppend}\n`, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve(fileToAppend)
                console.log(`${fileToAppend} has been appended successfully to ${fileName}`)
            }
        })
    })
}

function unlinkFile(fileNames) {
    return Promise.all(fileNames.map((fileName) => {
        return new Promise((resolve, reject) => {
            fs.unlink(path.resolve(__dirname, fileName), (err) => {
                if (err) {
                    reject(err)
                } else {
                    console.log(`${fileName} has been deleted successfully`)
                    resolve()
                }
            })
        })
    }))
}

function problem2(fileToBeRead, fileToAppend) {
    readFile(fileToBeRead)
        .then((data) => {
            const dataToUpperCase = data.toUpperCase()
            return writeFile('toUpperCase.txt', dataToUpperCase)
        })
        .then((fileName) => {
            return appendFile(fileToAppend, fileName)
        })
        .then((fileName) => {
            return readFile(fileName)
        })
        .then((data) => {
            const arrData = data
                .toLowerCase()
                .split('.')
                .map((sentence) => {
                    return sentence.trim().replace('\n', '')
                })
                .filter((sentence) => {
                    return sentence !== ""
                })
            return writeFile(`lowerCaseFile.json`, JSON.stringify(arrData))
        })
        .then((fileName) => {
            return appendFile(fileToAppend, fileName)
        })
        .then((fileName) => {
            return readFile(fileName)
        })
        .then((data) => {
            const toBeSorted = JSON.parse(data)
            toBeSorted.sort((sentence1, sentence2) => {
                if (sentence1 < sentence2) {
                    return -1
                }
                if (sentence2 < sentence1) {
                    return 1
                }
                return 0
            })
            return writeFile(`lowerCaseFileSorted.json`, JSON.stringify(toBeSorted))
        })
        .then((fileName) => {
            return appendFile(fileToAppend, fileName)
        })
        .then(() => {
            return readFile(fileToAppend)
        })
        .then((data) => {
            const splittedData = data.split('\n').filter((file) => {
                return file !== ""
            })
            return unlinkFile(splittedData)
        })
        .then(() => {
            return unlinkFile([fileToAppend])
        })
        .catch((err) => {
            console.error(err)
        })
}

module.exports = problem2