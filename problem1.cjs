const fs = require('fs')
const path = require('path')

function makeDir(dirname) {
    return new Promise((resolve, reject) => {
        fs.mkdir(path.join(__dirname, dirname), (err) => {
            if (!err || err.code === "EEXIST") {
                resolve(`dir ${dirname} has been created`)
            } else {
                reject(err)
            }
        })
    })
}

function writeFile(fileNames, dirname) {
    return Promise.all(fileNames.map((fileName) => {
        return new Promise((resolve, reject) => {
            fs.writeFile(path.resolve(__dirname, dirname, fileName), JSON.stringify({ message: `${fileName} has been created` }), (err) => {
                if (err) {
                    reject(err)
                } else {
                    console.log(`${fileName} has been created`)
                    resolve()
                }
            })
        })
    }))
}

function unlinkFile(fileNames, dirname) {
    return Promise.all(fileNames.map((fileName) => {
        return new Promise((resolve, reject) => {
            fs.unlink(path.resolve(__dirname, dirname, fileName), (err) => {
                if (err) {
                    reject(err)
                } else {
                    console.log(`${fileName} has been deleted`)
                    resolve()
                }
            })
        })
    }))
}

function removeDir(dirname) {
    return new Promise((resolve, reject) => {
        fs.rmdir(path.join(__dirname, dirname), (err) => {
            if (err) {
                reject(err)
            } else {
                resolve(`dir ${dirname} has been deleted`)
            }
        })
    })
}

function problem1(fileNames, dirname) {
    makeDir(dirname)
        .then((message) => {
            console.log(message)
            return writeFile(fileNames, dirname)
        })
        .then(() => {
            console.log(`All file have been written successfully`)
            return unlinkFile(fileNames, dirname)
        })
        .then(() => {
            console.log('All files has been deleted')
            return removeDir(dirname)
        })
        .then((message) => {
            console.log(message)
        })
        .catch((err) => {
            console.error(err)
        })
}

module.exports = problem1

